import matplotlib.pyplot as plot

import sys
import pickle


def main():
    print('Loading data...')
    with open('out.pkl', 'rb') as fl:
        data = pickle.load(fl)

    print('Generating plot data...')
    x = []
    y = []
    for gamma, values in data:
        x += [gamma] * len(values)
        y += values

    print('Plotting...')
    plot.scatter(x, y, marker='.', s=0.05, color='black')
    plot.show()


if __name__ == '__main__':
    sys.exit(main())
