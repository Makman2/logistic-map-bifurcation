# logistic-map-bifurcation

Renders bifurcation plots for the logistic function.
See
https://en.wikipedia.org/wiki/Logistic_map#/media/File:Subsection_Bifurcation_Diagram_Logistic_Map.png

## Structure

This repo has a Python version and a speed-optimized Vala version.
The Python version is ideal for experimenting and investigating data
interactively, while the Vala version renders fixed-sized pictures,
but way faster.

- Python version

  Consists of `datagen.py` and `dataview.py`. Former one generates all
  data, while later one just views it. The `pickle` module is utilized
  to store the data in `out.pkl`. Beware: Memory consumption can become
  very high for very high bifurcation plot resolutions.

- Vala version

  Contained in `highres`. You can build it from the toplevel directory with
  Meson. Places images into `out.png`.
