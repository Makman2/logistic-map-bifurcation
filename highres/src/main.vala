using Math;
using Cairo;

[Compact]
struct Point {
    public double x;
    public double y;

    public Point() {
        this.x = 0.0;
        this.y = 0.0;
    }

    public Point.from_values(double x, double y) {
        this.x = x;
        this.y = y;
    }
}

[Compact]
struct Domain {
    public Point start;
    public Point end;

    public double width {
        get {
            return this.end.x - this.start.x;
        }
    }

    public double height {
        get {
            return this.end.y - this.start.y;
        }
    }

    public Domain() {
        this.start = Point();
        this.end = Point();
    }

    public Domain.from_values(double x1, double y1, double x2, double y2) {
        this.start = Point.from_values(x1, y1);
        this.end = Point.from_values(x2, y2);
    }
}

struct RGBBuffer {
    public uchar[] data;

    public int width {
        get;
        private set;
    }

    public int height {
        get;
        private set;
    }

    public int stride {
        get;
        private set;
    }

    public RGBBuffer(int width, int height) {
        this.stride = Format.RGB24.stride_for_width(width);
        this.width = width;
        this.height = height;
        this.data = new uchar[this.stride * height];
    }

    public static uchar convert_float_color_to_byte(double color) {
        return (uchar)(255 * color.clamp(0.0, 1.0));
    }

    public void set_pixel(int x, int y, double r, double g, double b) {
        // Although the format is RGB24, Cairo always takes in RGBA, while A
        // gets ignored. Not documented at all! Also the format appears to
        // be BGRA as well, so opposite direction. People mentioned that it
        // depends on system endianness.
        var addr = y * this.stride + x * 4;
        data[addr] = convert_float_color_to_byte(b);
        data[addr+1] = convert_float_color_to_byte(g);
        data[addr+2] = convert_float_color_to_byte(r);
    }

    public ImageSurface to_cairo_surface() {
        return new ImageSurface.for_data(
            this.data,
            Format.RGB24,
            this.width,
            this.height,
            this.stride
        );
    }
}

public delegate double MapFunc(double input);
public delegate bool CountFunc(double testval);
public delegate void Action(int column, int row, double val);

public class RectangularValueBuffer : Object {
    public double[,] data;

    public int width {
        get {
            return this.data.length[0];
        }
    }

    public int height {
        get {
            return this.data.length[1];
        }
    }

    public RectangularValueBuffer(int width, int height) {
        this.data = new double[width, height];
    }

    public void set_value(int x, int y, double val) {
        this.data[x, y] = val;
    }

    public double get_value(int x, int y) {
        return this.data[x, y];
    }

    public void map(MapFunc mapfunc) {
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                this.data[x, y] = mapfunc(this.data[x, y]);
            }
        }
    }

    public void map_column(int column, MapFunc mapfunc) {
        for (int y = 0; y < this.height; y++) {
            this.data[column, y] = mapfunc(this.data[column, y]);
        }
    }

    public int count(CountFunc countfunc) {
        int count = 0;
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                if (countfunc(this.data[x, y])) {
                    count++;
                };
            }
        }
        return count;
    }

    public int count_in_column(int column, CountFunc countfunc) {
        int count = 0;
        for (int y = 0; y < this.height; y++) {
            if (countfunc(this.data[column, y])) {
                count++;
            }
        }
        return count;
    }

    public void @foreach(Action action) {
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                action(x, y, this.data[x, y]);
            }
        }
    }

    public double column_min(int column) {
        double x = double.MAX;
        for (int y = 0; y < this.height; y++) {
            if (x != 0.0) {  // Not quite a min function in usual sense, but better for our purpose.
                x = double.min(this.data[column, y], x);
            }
        }
        return x;
    }

    public double column_max(int column) {
        double x = double.MIN;
        for (int y = 0; y < this.height; y++) {
            x = double.max(this.data[column, y], x);
        }
        return x;
    }
}

public class LogisticMap {
    public LogisticMap(double gamma) {
        this.gamma = gamma;
    }

    double gamma {
        public get;
        private set;
    }

    public double eval(double x) {
        return this.gamma * x * (1.0 - x);
    }

    public Series series(double x0) {
        return new Series(this, x0);
    }

    public class Series {
        private LogisticMap map;
        private double x;

        public Series(LogisticMap map, double x0) {
            this.map = map;
            this.x = x0;
        }

        public double next() {
            this.x = this.map.eval(this.x);
            return this.x;
        }

        public Iterator iterator() {
            return new Iterator(this);
        }

        public class Iterator {
            private Series series;

            public Iterator(Series series) {
                this.series = series;
            }

            public bool next() {
                return true;
            }

            public double get() {
                return this.series.next();
            }
        }
    }
}

Point remap_point(Domain source_domain, Domain target_domain, Point pt) {
    var remapped = Point();

    remapped.x = (pt.x - source_domain.start.x) / source_domain.width *
        target_domain.width + target_domain.start.x;
    remapped.y = (pt.y - source_domain.start.y) / source_domain.height *
        target_domain.height + target_domain.start.y;

    return remapped;
}

class Worker {
    public Domain domain { get; private set; }
    public Domain image_domain { get; private set; }
    public AsyncQueue<Result> result_queue { get; private set; }
    public int image_x { get; private set; }
    public double x0 { get; private set; }
    public int preconvergence_iterations { get; private set; }
    public int iterations { get; private set; }

    public class Result : Object {
        public int image_x;
        public double[] data;
    }

    public Worker (int image_x,
                   double x0,
                   Domain domain,
                   Domain image_domain,
                   AsyncQueue<Result> result_queue,
                   int preconvergence_iterations,
                   int iterations) {
		this.image_x = image_x;
		this.x0 = x0;
		this.domain = domain;
		this.image_domain = image_domain;
		this.result_queue = result_queue;
		this.preconvergence_iterations = preconvergence_iterations;
		this.iterations = iterations;
    }

    private static double max(double[] data) {
        double x = double.MIN;
        for (int y = 0; y < data.length; y++) {
            x = double.max(data[y], x);
        }
        return x;
    }

    private static double min(double[] data) {
        double x = double.MAX;
        for (int y = 0; y < data.length; y++) {
            if (data[y] != 0.0) {
                x = double.min(data[y], x);
            }
        }
        return x;
    }

	public void run () {
	    double[] image_data = new double[(int)(image_domain.end.y)];

        double gamma = remap_point(
            this.image_domain,
            this.domain,
            Point.from_values(this.image_x, 0)
        ).x;

        var logistic_map = new LogisticMap(gamma);
        var series = logistic_map.series(this.x0);

        // Pass a few initial iterations to get the converged values.
        for (int i = 0; i < this.preconvergence_iterations; i++) {
            series.next();
        }

        int i = 0;
        foreach (var val in series) {
            if (i >= this.iterations) {
                break;
            }

            int remapped_val = (int)(remap_point(
                this.domain,
                this.image_domain,
                Point.from_values(gamma, val)
            ).y);

            if (remapped_val >= 0 && remapped_val < image_data.length) {
                image_data[remapped_val] += 1.0;
            }

            i++;
        }

        var data_min = min(image_data);
        var data_max = max(image_data);
        for (int n = 0; n < image_data.length; n++) {
            image_data[n] = 1 - (image_data[n] - data_min) / (data_max - data_min);
        }

        var result = new Result();
        result.image_x = image_x;
        result.data = image_data;
        this.result_queue.push(result);
    }
}

int main(string[] args) {
    // ---- CONFIGURATION AREA ----

    // The maximum range for gamma is [0, 4]. Beyond that interval the logistic map gets divergent.
    // Out[24]: (0.4847269153, 0.4847273158)
    Domain domain = Domain.from_values(3.8521864292, 0.484727030644, 3.8521864296, 0.484727040256);
    int preconvergence_iterations = 10000;
    int iterations = 1000000;
    int[] image_dimensions = {1920, 1080};  // 1K
    //int[] image_dimensions = {3840, 2160};  // 4K
    //int[] image_dimensions = {7680, 4320};  // 16K
    //int[] image_dimensions = {15360, 8640};  // 32K
    double x0 = 0.5;
    int threads = 8;

    // ---- CONFIGURATION AREA END ----

    print(@"Running multithreaded with $threads threads.\n");

    var image_domain = Domain.from_values(0, 0, image_dimensions[0], image_dimensions[1]);

    var image_data = new RectangularValueBuffer(image_dimensions[0], image_dimensions[1]);

    try {
        ThreadPool<Worker> pool = new ThreadPool<Worker>.with_owned_data((worker) => {
            // Call worker.run () on thread-start
            worker.run();
        }, 8, false);

        var results = new AsyncQueue<Worker.Result>();

        // We iterate over all gamma values that fit into the image (in x direction obviously).
        for (int image_x = 0; image_x < image_dimensions[0]; image_x++) {
            var task = new Worker(
                image_x,
                x0,
                domain,
                image_domain,
                results,
                preconvergence_iterations,
                iterations
            );

            pool.add(task);
        }

        int received_results = 0;
        while (received_results < image_dimensions[0]) {
            var result = results.pop();
            received_results++;

            // Copy to image data.
            for (int i = 0; i < result.data.length; i++) {
                image_data.set_value(result.image_x, i, result.data[i]);
            }

            print(@"Status: $received_results/$(image_dimensions[0])");
            print(@" [$(100*received_results/image_dimensions[0])%]\r");
        }
        print(@"Status: $(image_dimensions[0])/$(image_dimensions[0]) [100%]");
    } catch (ThreadError e) {
        print ("\nThreadError: %s\n", e.message);
        return 1;
    }

    print("\n");

    print("Saving image...\n");

    // Convert to image. Especially flip it so it is a proper graph.
    var image =  RGBBuffer(image_dimensions[0], image_dimensions[1]);
    image_data.@foreach((x, y, val) => {image.set_pixel(x, image.height-y-1, val, val, val);});

    image.to_cairo_surface().write_to_png("out.png");

    print("Finished.\n");

    return 0;
}
