from bisect import bisect
from math import isclose

import numpy as np

import pickle
import sys


MAX_ITERATIONS = 1024  # max calls for the logistic map per starting point x0.
RANGE = (0, 4)  # gamma beyond 4 is divergent.
RESOLUTION = 1000  # gamma testing resolution
RELATIVE_TEST_TOLERANCE = 1e-7


def logistic_map(gamma, x):
    return gamma * x * (1 - x)


def main():
    master_data = []

    gamma_start, gamma_end = RANGE
    for i, gamma in enumerate(np.linspace(gamma_start, gamma_end, RESOLUTION)):
        print(f'Checking {i+1}/{RESOLUTION}, gamma = {gamma}...')

        x_next = 0.5
        data = [x_next]
        for _ in range(MAX_ITERATIONS):
            x_next = logistic_map(gamma, x_next)
            data.append(x_next)

        # We do an after analysis on periodic convergence. A bit easier.
        periodic_points = []
        for d in reversed(data):
            sorted_index = bisect(periodic_points, d)
            if ((sorted_index < len(periodic_points) and isclose(d, periodic_points[sorted_index],
                                                                 rel_tol=RELATIVE_TEST_TOLERANCE)) or
                (sorted_index > 0 and isclose(d, periodic_points[sorted_index-1],
                                              rel_tol=RELATIVE_TEST_TOLERANCE))):
                break
            periodic_points.insert(sorted_index, d)
        master_data.append((gamma, periodic_points))

    filename = 'out.pkl'
    print(f'Saving to {filename}...')
    with open(filename, 'wb') as fl:
        pickle.dump(master_data, fl)


if __name__ == '__main__':
    sys.exit(main())
